function document_OnReady()
{
	debugPopulate()
}

$(document).ready(document_OnReady);

function debugPopulate()
{
	addSchool("Binghampton");
	addSchool("SBU");
	addSchool("Stony Brook");
	addSchool("Stony Brook University");
}

//adds a list item to school list element, and adds checkbox child to list with id == identifier
function addSchool(identifier)
{
	var schoolList = document.getElementById("School List");
	
	var baseListItem = document.createElement("li");
	baseListItem.className="list-group-item";
	
	var checkBox = document.createElement("div");
	checkBox.className="checkbox";
	var checkBoxInput = document.createElement("input");
	checkBoxInput.type="checkbox";
	checkBoxInput.id=identifier;
	checkBox.appendChild(checkBoxInput);
	var checkBoxLabel = document.createElement("label");
	checkBoxLabel.innerHTML=identifier;
	checkBox.appendChild(checkBoxLabel);
	baseListItem.appendChild(checkBox);
	schoolList.appendChild(baseListItem);
}
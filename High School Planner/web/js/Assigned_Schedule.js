 /*
	NOTE: numDays and numPeriods is the max number of periods / days per each semester (i assumed you would not view semesters at the same time)
 */
 var numDays = 4;
 var numPeriods = 7;
 
 var currentCourseCount = 0;
 
function document_OnReady()
{
	$("#EnterCoursesButton").click(
            function(){
             $(".Class_Info_Section").show();   
            }   
            );
           
}

$(document).ready(document_OnReady);

function addClassInfoPanel()
{
	currentCourseCount++;
	var classInfoSectionDiv = document.getElementsByClassName("Class_Info_Section")[0];
	var classPanelDiv = document.createElement("div");
	
	var baseLabel = document.createElement("p");
	baseLabel.innerHTML= "Course " + currentCourseCount + ":"
	classPanelDiv.appendChild(baseLabel);
	
	//TO DO:
	//the labels are supposed to be next to fields, not above
	//children of form-groups need different class names
	//bootstrap -> horizontal form
	
	var courseInfoForm = document.createElement("form");
	courseInfoForm.className="form-horizontal";

	var scheduleBlockGroup = document.createElement("div");
	scheduleBlockGroup.className="form-group";
	var scheduleBlockLabel = document.createElement("label");
	scheduleBlockLabel.innerHTML="Schedule Block: ";
	var scheduleBlockDropdown = $("select");
	var i; var j;
	for(i = 0; i < numDays; i++)
	{
		for(j = 0; j < numPeriods; j++)
		{
			var dropdownOption = document.createElement("option");
			dropdownOption.innerHTML = "Period " + (j + 1) + 
                                ",Day " + (i + 1);
			scheduleBlockDropdown.appendChild(dropdownOption);
		}
	}
	scheduleBlockGroup.appendChild(scheduleBlockLabel);
	scheduleBlockGroup.appendChild(scheduleBlockDropdown);
	courseInfoForm.appendChild(scheduleBlockGroup);
	
	var courseIDGroup = document.createElement("div");
	courseIDGroup.className="form-group";
	var courseIDLabel = document.createElement("label");
	courseIDLabel.innerHTML="Course ID: ";
	var courseIDField = document.createElement("input");
	courseIDField.className="form-control";
	courseIDField.type="text";
	courseIDField.id="courseID";
	courseIDGroup.appendChild(courseIDLabel);
	courseIDGroup.appendChild(courseIDField);
	courseInfoForm.appendChild(courseIDGroup);
	
	var courseNameGroup = document.createElement("div");
	courseNameGroup.className="form-group";
	var courseNameLabel = document.createElement("label");
	courseNameLabel.innerHTML="Course Name: ";
	var courseNameField = document.createElement("input");
	courseNameField.className="form-control";
	courseNameField.type="text";
	courseNameField.id="courseName";
	courseNameGroup.appendChild(courseNameLabel);
	courseNameGroup.appendChild(courseNameField);
	courseInfoForm.appendChild(courseNameGroup);
	
	var instructorNameGroup = document.createElement("div");
	instructorNameGroup.className="form-group";
	var instructorNameLabel = document.createElement("label");
	instructorNameLabel.innerHTML="Instructor Name: ";
	var instructorNameField = document.createElement("input");
	instructorNameField.className="form-control";
	instructorNameField.type="text";
	instructorNameField.id="instructorName";
	instructorNameGroup.appendChild(instructorNameLabel);
	instructorNameGroup.appendChild(instructorNameField);
	courseInfoForm.appendChild(instructorNameGroup);
	
	classPanelDiv.appendChild(courseInfoForm);
	classInfoSectionDiv.appendChild(classPanelDiv);
}

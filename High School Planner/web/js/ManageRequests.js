function document_OnReady()
{
	debugPopulateRequests();
}

$(document).ready(document_OnReady);

function debugPopulateRequests()
{
	addRequest("Tom - SBU", 108122235);
	addRequest("Richa - SBU", 784287522);
	addRequest("Brian - SBU", 92839234);
	addRequest("Angela - SBU", 183941231);
}

function addRequest(identifier, id)
{
	var list = document.getElementById("RequestList");
	
	var listItem = document.createElement("li");
	listItem.className="list-group-item";
	listItem.id = id;
	
	var acceptButton = document.createElement("button");
	acceptButton.className="btn btn-default";
	acceptButton.innerHTML="Accept";
	var ignoreButton = document.createElement("button");
	ignoreButton.className="btn btn-default";
	ignoreButton.innerHTML="Ignore";
	
	listItem.innerHTML = identifier;
	listItem.appendChild(acceptButton);
	listItem.appendChild(ignoreButton);
	list.appendChild(listItem);	
}

function document_OnReady()
{
	debugPopulate();
}

$(document).ready(document_OnReady);

function debugPopulate()
{
	addFriendToList("Tom");
	addFriendToList("Angela");
	addFriendToList("Brian");
	addFriendToList("Richa");
}

function addStudentToResultsList(name)
{
	var friendList = document.getElementById("FriendsList");
	
	var radioDiv = document.createElement("div");
	radioDiv.className="radio";
	var friendLabel = document.createElement("label");
	var input = document.createElement("input");
	input.type="radio";
	input.name="FriendsList";
	friendLabel.innerHTML=name;
	radioDiv.appendChild(input);
	radioDiv.appendChild(friendLabel);
	friendList.appendChild(radioDiv);
}

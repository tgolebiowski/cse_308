/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(document_OnReady);

function document_OnReady()
{
    $(".approveAccountLink").click(AddAccountsToApprove);
    $("#doneManaging").click(ApproveAccounts);
    $(".deleteAccountLink").click(AddAccountsToDelete);
    $("#ApproveAll").click(ApproveAllAccounts);
}

var accountsToApprove = [];
var accountsToDelete = [];
var allAccounts = []
function AddAccountsToApprove()
{
    //Get the data-studentId of the selected account
    //and push it in the list of the accounts to approve
    accountsToApprove.push($(this).closest("div").attr("data-studentId"));
}

function AddAccountsToDelete()
{
    //Get the data-studentId of the selected account
    //and push it in the list of the accounts to delete
    accountsToDelete.push($(this).closest("div").attr("data-studentId"));
}

function ApproveAccounts()
{
   $.ajax({
            url:"ApproveAccountsServlet",
            type:"POST",
            dataType:'json',
            data: {approveAccountList: accountsToApprove
                , deleteAccountList: accountsToDelete
            },
            success:function(data){
               alert("accounts added and deleted" +data);
            }
    });
}

function ApproveAllAccounts()
{
   var accounts = $(".studentAccount");
   $.each(accounts, function(i, val){
       allAccounts.push($(this).attr("data-studentId"));
       
   });
   $.ajax({
            url:"ApproveAccountsServlet",
            type:"POST",
            dataType:'json',
            data: {approveAccountList: allAccounts
                
            },
            success:function(data){
               alert("all added");
            }
    });
}
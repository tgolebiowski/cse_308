function document_OnReady()
{
	debugPopulate();
}

$(document).ready(document_OnReady);

function debugPopulate()
{
	
}

function addFriendToList(identifier)
{
	var friendsList = document.getElementById("Friends List");
	
	var listItem = document.createElement("li");
	listItem.className="list-group-item";
	
	listItem.innerHTML=identifier;
	
	var buttonGroup = document.createElement("div");
	buttonGroup.className="btn-group btn-group-xs";
	var assignedScheduleButton = document.createElement("button");
	assignedScheduleButton.type="button";
	assignedScheduleButton.className="btn btn-primary";
	assignedScheduleButton.id=identifier + " - assigned";
	assignedScheduleButton.innerHTML="View Assigned Schedule";
	var generatedScheduleButton = document.createElement("button");
	generatedScheduleButton.type="button";
	generatedScheduleButton.className="btn btn-primary";
	generatedScheduleButton.id=identifier + " - generated";
	generatedScheduleButton.innerHTML="View Generated Schedule";
	buttonGroup.appendChild(assignedScheduleButton);
	buttonGroup.appendChild(generatedScheduleButton);
	listItem.appendChild(buttonGroup);
	friendsList.appendChild(listItem);
}

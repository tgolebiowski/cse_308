function document_OnReady()
{
	debugPopulate();
}

$(document).ready(document_OnReady);

function debugPopulate()
{
	appendCourse("CSE 308");
	appendCourse("CSE 306");
	appendCourse("CSE 307");
	appendCourse("CSE 99999999");
}

function appendCourse(identifier)
{
	var courseList = document.getElementById("Course List");
	
	var baseListItem = document.createElement("li");
	baseListItem.className="list-group-item";
	baseListItem.innerHTML=identifier;
	courseList.appendChild(baseListItem);
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 
 /*
	Tom copied the basics from richa
 */

 /*
	NOTE: numDays and numPeriods is the max number of periods / days per each semester (i assumed you would not view semesters at the same time)
 */
 var numDays = 7;
 var numPeriods = 7;
 
function document_OnReady()
{
	var scheduleDiv = document.getElementsByClassName("schedule")[0];
	var i;
	var dynamicChar = 1;
	for(i = 0; i < numDays; i++)
	{
		var newScheduleColumn = document.createElement("div");
		newScheduleColumn.className = "schedule_column";
		
		var dayLabel = document.createElement("label");
		dayLabel.innerHTML = "Day: " + (i + 1);
		
		var periodList = document.createElement("ul");
		periodList.className = "list-group";
		
		newScheduleColumn.appendChild(dayLabel);
		newScheduleColumn.appendChild(periodList);
		scheduleDiv.appendChild(newScheduleColumn);
		
		var j;
		for(j = 0; j < numPeriods; j++)
		{
			var periodElement = document.createElement("li");
			periodElement.className = "list-group-item";
			
			if(j == 0)
			{
				periodElement.innerHTML = "CSE 373";
			}
			else if(j == 1)
			{
				periodElement.innerHTML = "CSE 220";
			}
			else if(j == 2)
			{
				periodElement.innerHTML = "Lunch";
			}
			else if(j == 3)
			{
				periodElement.innerHTML = "CSE 310";
			}
			else if(j == 4)
			{
				periodElement.innerHTML = "ACH 101";
			}
			else if(j == 5)
			{
				periodElement.innerHTML = "CSE 219";
			}
			else
			{
				periodElement.innerHTML = "CSE 308";
			}
			
			periodList.appendChild(periodElement);
		}
	}
}

$(document).ready(document_OnReady);

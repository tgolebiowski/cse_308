<%-- 
    Document   : home
    Created on : Mar 24, 2015, 12:23:01 AM
    Author     : tomaszGolebiowski
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin Manage Schools</title>
        <link href = "css/bootstrap.min.css" rel = "stylesheet">
        <link href = "css/styles.css" rel = "stylesheet">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
   <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src = "js/bootstrap.js"></script>
        <script src = "js/bootstrap.min.js"></script>
		<script src = "js/Admin_Schools_Delete.js"></script>
    </head>
    <body>
        <div class = "navbar navbar-inverse navbar-static-top">
            <div class = "container">
                <div class = "navbar-header">
                    <a href = "#" class = "navbar-brand">High School Planner: Admin - Delete Schools</a>
                </div>
            </div>
			
			 <div class = "collapse navbar-collapse navHeaderCollapse">

                    <ul class = "nav navbar-nav navbar-right">

                        <li class = "active"><a href = "#">Delete Schools</a></li>

                        <!--<li class = "dropdown">
            
                            <a href = "#" class = "dropdown-toggle" data-toggle = "dropdown">Projects<b class = "caret"></b></a>
                            <ul class = "dropdown-menu" role="menu">
                              <li><a href = "appinventor.html">Dropdown example</a></li>
                            </ul>
            
                        </li>-->

                        <li><a href = "Admin_Home.jsp">Dashboard</a></li>

                    </ul>

                </div>
        </div>   

		<div class="School-List-block" role="form">
			<ul class="list-group" id="School List">
			</ul>
		</div>
		
		<div class = "Buttons div">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="delete" class="btn btn-default">Delete</button>
			</div>
		</div>
    </body>
</html>
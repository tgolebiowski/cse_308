<%-- 
    Document   : home
    Created on : Mar 24, 2015, 12:23:01 AM
    Author     : tomaszGolebiowski
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Student Home</title>
        <link href = "css/bootstrap.min.css" rel = "stylesheet">
        <link href = "css/styles.css" rel = "stylesheet">
        <link href = "css/Assigned_Schedule.css" rel = "stylesheet">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">

        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src = "js\bootstrap.js"></script>
        <script src = "js\Assigned_Schedule.js"></script>
    </head>
    <body>
      
        <div class = "navbar navbar-inverse navbar-static-top">
            <div class = "container">
                <div class = "navbar-header">

                    <a href = "#" class = "navbar-brand">High School Planner - Enter Asssigned Schedule</a>
                    <button class = "navbar-toggle" data-toggle = "collapse" data-target = ".navHeaderCollapse">
                        <span class = "icon-bar"></span>
                    </button>

                </div>
            </div>
        </div>

        <div class="Class_Info_Section">
            
        <div class="Button_Block">
            <button type="Add Another" class="btn btn-default">Add Another</button>
            <button type="Submit" class="btn btn-default">Submit</button>
        </div>
    </body>
</html>

<%-- 
    Document   : AssignedSchedule
    Created on : Mar 24, 2015, 12:42:41 AM
    Author     : richaprajapati
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Assigned Schedule</title>
        <link rel="stylesheet" type="text/css" href="Student_Home.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="Student_Home.js"></script>
    </head>
    <body>
      <form action="EnterAssignedScheduleServlet" method="POST">
        <div id="AssignedScheduleBlock">
            <div class="CourseIdInput">
                <label>Course ID: </label>
                <input type="text" name="courseId">
            </div>
            <div class="CourseNameInput">
                <label>Course Name: </label>
                <input type="text" name="courseName">
            </div>
            <div class="CourseNameInput">
                <label>Course Name: </label>
                <input type="text" name="courseName">
            </div>
            <div class="SchoolYearInput">
                <label>School Year: </label>
                <input type="text" name="schoolYear">
            </div>
            <div class="SemesterInput">
                <label>Semesters: </label>
                <span>
                    <select name="startSemester">
                        <option value="F2014">Fall 2014</option>
                        <option value="S2015">Spring 2015</option>
                        <option value="F2015">Fall 2015</option>
                        <option value="S2016">Spring 2016</option>
                    </select>
                     &nbsp;to&nbsp;
                     <select name="endSemester">
                        <option value="F2014">Fall 2014</option>
                        <option value="S2015">Spring 2015</option>
                        <option value="F2015">Fall 2015</option>
                        <option value="S2016">Spring 2016</option>
                    </select>
                </span>
            </div>
            <div class="timeInput">
                <span>
                <label>Time: </label>
                    <select>
                        <option value="P1D1">Period 1, Day 1</option>
                        <option value="P1D2">Period 1, Day 2</option>
                        <option value="P2D1">Period 2, Day 1</option>
                        <option value="P2D2">Period 2, Day 2</option>
                    </select>
                </span>
            </div>
            <div class="instructorInput">
                <label>Instructor: </label>
                <input type="text" name="InstructorName">
            </div>
        </div>
          <button type="submit">Submit</button>
       </form>
    </body>
</html>

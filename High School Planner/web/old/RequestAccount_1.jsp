<%-- 
    Document   : home
    Created on : Mar 24, 2015, 12:23:01 AM
    Author     : tomaszGolebiowski
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Student Home</title>
		<link href = "css/bootstrap.min.css" rel = "stylesheet">
        <link href = "css/styles.css" rel = "stylesheet">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
		
		<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src = "js\bootstrap.js"></script>
    </head>
    <body>
	
		<div class = "navbar navbar-inverse navbar-static-top">
			<div class = "container">
				<div class = "navbar-header">

                    <a href = "Student_Home.html" class = "navbar-brand">High School Planner</a>
                    <button class = "navbar-toggle" data-toggle = "collapse" data-target = ".navHeaderCollapse">
                        <span class = "icon-bar"></span>
                    </button>
					
                </div>

            </div>
        </div>
	
		<form class="form-horizontal" role="form" action="RequestAccountServlet">
			<div class="form-group">
				<label class="control-label col-sm-2" for="First Name">First Name:</label>
				<div class="col-sm-10">
					<input type="FirstName" class="form-control" name="FirstName" placeholder="First Name">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Last Name">Last Name:</label>
				<div class="col-sm-10">          
					<input type="Last Name" class="form-control" name="LastName" placeholder="Last Name">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="School">School:</label>
				<div class="col-sm-10">          
					<input type="School" class="form-control" name="SchoolName" placeholder="School">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Email">Email:</label>
				<div class="col-sm-10">          
					<input type="Email" class="form-control" name="Email" placeholder="Email">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Password">Password:</label>
				<div class="col-sm-10">          
					<input type="Password" class="form-control" name="Password" placeholder="Password">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="ConfirmedPassword">Confirm Password:</label>
				<div class="col-sm-10">          
					<input type="ConfirmedPassword" class="form-control" name="ConfirmedPassword" placeholder="ConfirmedPassword">
				</div>
			</div>
			<div class="form-group">        
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">Submit</button>
				</div>
			</div>
		</form>
    </body>
</html>
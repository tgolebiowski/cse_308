<%-- 
    Document   : Admin_Schools
    Created on : Mar 24, 2015, 4:35:19 PM
    Author     : mo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin Manage Schools</title>
        <link href = "css/bootstrap.min.css" rel = "stylesheet">
        <link href = "styles.css" rel = "stylesheet">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
   <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src = "js/boostrap.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class = "navbar navbar-inverse navbar-static-top">

            <div class = "container">


                <div class = "navbar-header">

                    <a href = "#" class = "navbar-brand">High School Planner</a>

                    <button class = "navbar-toggle" data-toggle = "collapse" data-target = ".navHeaderCollapse">

                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>

                    </button>

                </div>


                <div class = "collapse navbar-collapse navHeaderCollapse">

                    <ul class = "nav navbar-nav navbar-right">

                        <li class = "Admin_Home"><a href = "#">Dashboard</a></li>

                        <!--<li class = "dropdown">
            
                            <a href = "#" class = "dropdown-toggle" data-toggle = "dropdown">Projects<b class = "caret"></b></a>
                            <ul class = "dropdown-menu" role="menu">
                              <li><a href = "appinventor.html">Dropdown example</a></li>
                            </ul>
            
                        </li>-->

                        <li><a href = "Admin_ManageStudents.jsp">Manage Students</a></li>
                        


                    </ul>

                </div>

            </div>

        </div>

        <div class = "container">

            <h1 class = "center">
                Create New School
            </h1>
            <br>
            <p>*Please fill in all text fields</p>
            
            <br>
            <br>


        </div>
        <form action="AddSchoolServlet" method="POST">
        <div class="input-group">
            <span class="input-group-addon" id="schoolName">School Name:</span>
            <input name="schoolName" type="text" class="form-control" placeholder="Enter School Name" aria-describedby="basic-addon1">
        </div>
        <div class="input-group">
            <span class="input-group-addon" id="numOfSems">Number of Semesters:</span>
            <input name="numOfSemesters" type="text" class="form-control" placeholder="Enter # of semesters" aria-describedby="basic-addon1">
        </div>
        <div class="input-group">
            <span class="input-group-addon" id="num_days">Number of Days in a Schedule:</span>
            <input name="numOfDays" type="text" class="form-control" placeholder="Enter # of days" aria-describedby="basic-addon1">
        </div>
        <div class="input-group">
            <span class="input-group-addon" id="num_periods">Number of Periods in Each Day</span>
            <input name="numOfPeriods" type="text" class="form-control" placeholder="Enter # of periods" aria-describedby="basic-addon1">
        </div>
        <div class="input-group">
            <span class="input-group-addon" id="period_range">Range of Periods when Lunch if Offered</span>
            <input name="periodsRange" type="text" class="form-control" placeholder="Enter range... Ex. 1 - 2" aria-describedby="basic-addon1">
        </div>
        <p> Set of all Legal Schedule Blocks: <button type="button" class="btn btn-success">Add new Block</button>
        </p>
        
        
        <br>
        <button type="submit" class="btn btn-primary">Submit</button>

        <!--<div class = "navbar navbar-default navbar-static-bottom">

            <div class = "container">

                <p class = "navbar-text pull-left"></p>

            </div>

        </div>-->

        </form>
    </body>
</html>

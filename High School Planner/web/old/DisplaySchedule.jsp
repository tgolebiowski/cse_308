<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Student Home</title>
        <link rel="stylesheet" type="text/css" href="Student_Home.css">
		<link href = "css/bootstrap.min.css" rel = "stylesheet">
        <link href = "css/styles.css" rel = "stylesheet">
		<link href = "css/Desired_Schedule.css" rel = "stylesheet">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
		
		<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src = "js\bootstrap.js"></script>
		<script src = "js\Desired_Schedule.js"></script>
    </head>
    <body>
		<div class = "navbar navbar-inverse navbar-static-top">
			<div class = "container">
				<div class = "navbar-header">

                    <a href = "Student_Home.html" class = "navbar-brand">High School Planner</a>
                    <button class = "navbar-toggle" data-toggle = "collapse" data-target = ".navHeaderCollapse">
                        <span class = "icon-bar"></span>
                    </button>
					
                </div>


                <div class = "collapse navbar-collapse navHeaderCollapse">
                    <ul class = "nav navbar-nav navbar-right">
                        <li><a href = "Student_Home.html">Dashboard</a></li>
                        <li><a href = "#">Assigned Schedule</a></li>
						<li class = "active"><a href = "#">Desired Schedule</a></li>
						<li><a href = "#">View Friends</a></li>
						<li><a href = "Friend_Request.html">Send Friend Request</a></li>
                    </ul>
                </div>
            </div>
			</div>
        </div>
		
		<button type="button" class="btn btn-primary">View With Friends</button>
		<button type="button" class="btn btn-primary">View Without Friends</button>
		<div class = "schedule">
			
		</div>
    </body>
</html>

<%-- 
    Document   : DisplayFriends
    Created on : Mar 24, 2015, 2:02:10 AM
    Author     : richaprajapati
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Student Home</title>
		<link href = "css/bootstrap.min.css" rel = "stylesheet">
        <link href = "css/styles.css" rel = "stylesheet">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
		
		<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src = "js\bootstrap.js"></script>
    </head>
    <body>
	
		<div class = "navbar navbar-inverse navbar-static-top">
			<div class = "container">
				<div class = "navbar-header">

                    <a href = "Student_Home.html" class = "navbar-brand">High School Planner</a>
                    <button class = "navbar-toggle" data-toggle = "collapse" data-target = ".navHeaderCollapse">
                        <span class = "icon-bar"></span>
                    </button>
					
                </div>


                <div class = "collapse navbar-collapse navHeaderCollapse">
                    <ul class = "nav navbar-nav navbar-right">
                        <li><a href = "Student_Home.html">Dashboard</a></li>
                        <li class = "active"><a href = "Assigned_Schedule.html">Assigned Schedule</a></li>
						<li><a href = "#">Desired Schedule</a></li>
						<li><a href = "#">View Friends</a></li>
						<li><a href = "Friend_Request.html">Send Friend Request</a></li>
                    </ul>
                </div>
            </div>
        </div>
	
		<form action="RequestAccountServlet" method="POST" class="form-horizontal" role="form">
			<div class="form-group">
				<label class="control-label col-sm-2" for="First Name">First Name:</label>
				<div class="col-sm-10">
					<input name="FirstName" type="FirstName" class="form-control" id="FirstName" placeholder="First Name">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Last Name">Last Name:</label>
				<div class="col-sm-10">          
					<input name="LastName" type="Last Name" class="form-control" id="Last Name" placeholder="Last Name">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="School">School:</label>
				<div class="col-sm-10">          
					<input name="SchoolName" type="School" class="form-control" id="School" placeholder="School">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Email">Email:</label>
				<div class="col-sm-10">          
					<input name="Email" type="Email" class="form-control" id="Email" placeholder="Email">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Password">Password:</label>
				<div class="col-sm-10">          
					<input name="Password" type="Password" class="form-control" id="Password" placeholder="Password">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="ConfirmedPassword">Confirm Password:</label>
				<div class="col-sm-10">          
					<input name="ConfirmedPassword" type="ConfirmedPassword" class="form-control" id="ConfirmedPassword" placeholder="ConfirmedPassword">
				</div>
			</div>
			<div class="form-group">        
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">Submit</button>
				</div>
			</div>
		</form>
    </body>
</html>
</html>

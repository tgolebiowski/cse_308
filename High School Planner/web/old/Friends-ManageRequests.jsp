<%-- 
    Document   : home
    Created on : Mar 24, 2015, 12:23:01 AM
    Author     : tomaszGolebiowski
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Send Friend Request</title>
		<link href = "css/bootstrap.min.css" rel = "stylesheet">
        <link href = "css/styles.css" rel = "stylesheet">
		<link href = "css/Friend_Request.css" rel = "stylesheet">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
		
		<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src = "js/bootstrap.js"></script>
		<script src = "js/ManageRequests.js"></script>
		
    </head>
    <body>
		<div class = "navbar navbar-inverse navbar-static-top">
			<div class = "container">
				<div class = "navbar-header">

                    <a href = "#" class = "navbar-brand">High School Planner - Student</a>
                    <button class = "navbar-toggle" data-toggle = "collapse" data-target = ".navHeaderCollapse">
                        <span class = "icon-bar"></span>
                    </button>
					
                </div>


                <div class = "collapse navbar-collapse navHeaderCollapse">
                    <ul class = "nav navbar-nav navbar-right">
                        <li><a href = "#">Dashboard</a></li>
                        <li><a href = "#">Assigned Schedule</a></li>
						<li><a href = "#">Desired Schedule</a></li>
						<li><a href = "#">View Friends</a></li>
						<li class = "active"><a href = "#">Find Friends</a></li>
                    </ul>
                </div>
            </div>
        </div>
		
		<ul class="list-group" id="RequestList">
		
		</ul>
    </body>
</html>

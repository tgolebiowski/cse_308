<%-- 
    Document   : Admin_Home
    Created on : Mar 24, 2015, 3:50:19 AM
    Author     : mo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrator Home</title>
        <link href = "css/bootstrap.min.css" rel = "stylesheet">
        <link href = "styles.css" rel = "stylesheet">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    </head>
    <body>
        <div class = "navbar navbar-inverse navbar-static-top">

            <div class = "container">


                <div class = "navbar-header">

                    <a href = "#" class = "navbar-brand">High School Planner</a>

                    <button class = "navbar-toggle" data-toggle = "collapse" data-target = ".navHeaderCollapse">

                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>

                    </button>

                </div>


                <div class = "collapse navbar-collapse navHeaderCollapse">

                    <ul class = "nav navbar-nav navbar-right">

                        <li class = "active"><a href = "#">Dashboard</a></li>

                        <!--<li class = "dropdown">
            
                            <a href = "#" class = "dropdown-toggle" data-toggle = "dropdown">Projects<b class = "caret"></b></a>
                            <ul class = "dropdown-menu" role="menu">
                              <li><a href = "appinventor.html">Dropdown example</a></li>
                            </ul>
            
                        </li>-->

                        <li><a href = "Admin_ManageSudents.jsp">Manage Students</a></li>
                        


                    </ul>

                </div>

            </div>

        </div>

        <div class = "container">

            <h1 class = "center">
                Admin Dashboard
            </h1>

            <br>
            <br>


        </div>
        
        <div class = "btn-group-vertical" role ="group" aria-label ="...">
            <button id="SchoolButton" type="button" class="btn btn-default">Schools</button>
            <button id="ManageStudentButton"type="button" class="btn btn-default">Manage Students</button>
            
        </div>

        <!--<div class="loginInfo" style="">
            <form>
                <div class="form-group">
                    <label for="inputUsername">Username:</label>
                    <input type="text" class="form-control" id="usr" placeholder="Username">
                </div>
                <div class="form-group">
                    <label for="inputPassword">Password</label>
                    <input type="password" class="form-control" id="inputPassword" placeholder="Password">
                </div>
                <div class="checkbox">
                    <label><input type="checkbox"> Remember Me</label>
                </div>
                <button type="submit" class="btn btn-primary">Login</button>
            </form>
        </div>-->


        <!--<div class = "navbar navbar-default navbar-static-bottom">

            <div class = "container">

                <p class = "navbar-text pull-left"></p>

            </div>

        </div>-->

        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src = "js/boostrap.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>

<%-- 
    Document   : home
    Created on : Mar 24, 2015, 12:23:01 AM
    Author     : tomaszGolebiowski
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Friends</title>
		<link href = "css/bootstrap.min.css" rel = "stylesheet">
        <link href = "css/styles.css" rel = "stylesheet">
		<link href = "css/Display_Friends.css" rel = "stylesheet">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
		
		<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src = "js\bootstrap.js"></script>
		<script src = "js/Friends-Display.js"></script>
    </head>
    <body>
		 <div class = "collapse navbar-collapse navHeaderCollapse">
                    <ul class = "nav navbar-nav navbar-right">
                        <li><a href = "Student_Home.html">Dashboard</a></li>
                        <li><a href = "#">Assigned Schedule</a>
                            
                            <div>
                                <a href="EnterAssigned_Step1.jsp">
                            <button type="button" id="EnterCoursesButton" class="btn btn-default" style="background: #222;
  float: left;
  color: #adadad;
  border: none;
  margin-left: -7px;display:none;">View Assigned Schedule</button></a>
                                <a href="ViewScheduleServlet"><button type="button" id="EnterCoursesButton" class="btn btn-default"  style="background: #222;
  float: left;
  margin-top: 39px;
  margin-left: -177px;
  color: #adadad;
  border: none;display:none;">Enter Assigned Schedule</button></a>
                            </div>
                        </li>
                        <li><a href = "#">Desired Schedule</a>
                       
                        </li>
			<li><a href = "#">View Friends</a></li>
			<li><a href = "Friend_Request.html">Send Friend Request</a></li>
                    </ul>
                </div>
        </div>
		<div class="Friend-List-block">
			<ul class="list-group" id="Friends List">
                            
			</ul>
		</div>
    </body>
</html>
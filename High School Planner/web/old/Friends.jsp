<%-- 
    Document   : Friends
    Created on : Mar 24, 2015, 12:28:51 AM
    Author     : richaprajapati
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage Friends</title>
        <link rel="stylesheet" type="text/css" href="Student_Home.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="Student_Home.js"></script>
    </head>
    <body>
        <label>Find Friends</label>
        <form action="FindFriendsServlet" method="POST">
            <div id="FindFriendsBlock">
                <span>
                    <label>First Name:</label>
                    <input name="FirstName" type="text">
                </span>
                <span>
                    <label>Last Name:</label>
                    <input name="LastName"type="text">
                </span>
                <button type="submit">Find</button>
             </div>
        </form>
        <div id="FriendSearchResult">
            <div>
                <span class="FoundFriends">Richa Prajapati, Stony Brook University</span>
                <button class="SendRequest" ype="button">Send Friend Request</button>
                <button class="RequestSent">Friend Request Sent</button>
                <button class="CancelRequest">Cancel Friend Request </button>
            </div>
            <div>
                <span class="FoundFriends">Richa Prajapati, NYU</span>
                <button class="SendRequest" type="button">Send Friend Request</button>
                <button class="RequestSent">Friend Request Sent</button>
                <button class="CancelRequest">Cancel Friend Request </button>
            </div>
            
    </body>
</html>

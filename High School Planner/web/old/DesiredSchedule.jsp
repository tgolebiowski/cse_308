<%-- 
    Document   : DesiredSchedule
    Created on : Mar 24, 2015, 12:43:11 AM
    Author     : richaprajapati
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Desired Schedule</title>
        <link rel="stylesheet" type="text/css" href="Student_Home.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="Student_Home.js"></script>
    </head>
    <body>
        <div id="DesiredScheduleBlock">
            <div id="PreferredCourses">
                <div id="courses">
                    <div class="CourseDetails">
                         <span>
                             <label>Courses: </label>
                             <input name="CourseNames" type="text">
                        </span>
                        <div>
                             <label>Exclude Sections:</label>
                             <input name="ExcludeSections" type="checkbox">Section 1
                             <input name="ExcludeSections" type="checkbox">Section 2
                             <input name="ExcludeSections" type="checkbox">Section 3
                             <input name="ExcludeSections" type="checkbox">Section 4
                             <input name="ExcludeSections" type="checkbox">Section 5
                        </div>
                        <div>
                             <label>Preferred Instructor: </label>
                             <input type="text" name="PreferredInstructor">
                        </div>
                    </div>
                </div>
                <button id="PlusButton" type="button">+</button>
                <div id="LunchPreferred">
                    <label>Lunch</label>
                     <div>
                        <label id="Monday">Monday: </label>
                        <select>
                             <option value="P1D1">Period 1, Day 1</option>
                             <option value="P1D2">Period 1, Day 2</option>
                             <option value="P2D1">Period 2, Day 1</option>
                             <option value="P2D2">Period 2, Day 2</option>
                        </select>
                    </div>
                    <div>
                        <label id="Tuesday">Tuesday </label>
                        <select>
                             <option value="P1D1">Period 1, Day 1</option>
                             <option value="P1D2">Period 1, Day 2</option>
                             <option value="P2D1">Period 2, Day 1</option>
                             <option value="P2D2">Period 2, Day 2</option>
                        </select>
                    </div>
                    <div>
                        <label id="Wednesday">Wednesday </label>
                        <select>
                             <option value="P1D1">Period 1, Day 1</option>
                             <option value="P1D2">Period 1, Day 2</option>
                             <option value="P2D1">Period 2, Day 1</option>
                             <option value="P2D2">Period 2, Day 2</option>
                        </select>
                    </div>
                    <div>
                        <label id="Thursday">Thursday </label>
                        <select>
                             <option value="P1D1">Period 1, Day 1</option>
                             <option value="P1D2">Period 1, Day 2</option>
                             <option value="P2D1">Period 2, Day 1</option>
                             <option value="P2D2">Period 2, Day 2</option>
                        </select>
                    </div>
                    <div>
                        <label id="Friday">Friday </label>
                        <select>
                             <option value="P1D1">Period 1, Day 1</option>
                             <option value="P1D2">Period 1, Day 2</option>
                             <option value="P2D1">Period 2, Day 1</option>
                             <option value="P2D2">Period 2, Day 2</option>
                        </select>
                    </div>
                </div>
                
                
            </div>
            <button type="submit">Generate Schedule</button>
            <button type="button">Remove</button>
        </div>
    </body>
</html>

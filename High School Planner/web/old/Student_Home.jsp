<%-- 
    Document   : home
    Created on : Mar 24, 2015, 12:23:01 AM
    Author     : richaprajapati
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Student Home</title>
        <link rel="stylesheet" type="text/css" href="Student_Home.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="Student_Home.js"></script>
    </head>
    <body>
        
        <form method="POST" action="NewServlet1">
            Name: <input type="text" name="name" />
            <input type="submit" value="Add" />
        </form>
 
        
        <button id="ScheduleButton" type="button">Schedule</button>
        <form action="AssignedSchedule.jsp" method="POST">
            <button id="AssignedScheduleButton" type="submit">AssignedSchedule</button>
        </form>
        <form action="DesiredSchedule.jsp" method="POST">
            <button id="DesiredScheduleButton" type="submit">DesiredSchedule</button>
        </form>

        <button id="FriendsButton" type="button">Friends</button>
        <form action="Friends.jsp" method="POST">
            <button id="ManageFriendsButton" type="submit">ManageFriends</button>
        </form>
        <form action="DisplayFriends.jsp" method="POST">
            <button id="DisplayFriendsButton" type="submit">Display Friends</button>
        </form>
    </body>
</html>

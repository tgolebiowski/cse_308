<%-- 
    Document   : DisplayFriends
    Created on : Mar 24, 2015, 2:02:10 AM
    Author     : tomaszgolebiowski
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Student Home</title>
		<link href = "css/bootstrap.min.css" rel = "stylesheet">
        <link href = "css/styles.css" rel = "stylesheet">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
		
		<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src = "js/bootstrap.js"></script>
		<script src = "js/Student-DisplayCourseList.js"></script>
    </head>
    <body>
		<div class = "navbar navbar-inverse navbar-static-top">
			<div class = "container">
				<div class = "navbar-header">

                    <a href = "Student_Home.html" class = "navbar-brand">Display Master Course List</a>
                    <button class = "navbar-toggle" data-toggle = "collapse" data-target = ".navHeaderCollapse">
                        <span class = "icon-bar"></span>
                    </button>
					
                </div>
            </div>
			</div>
        </div>
		<h2>Master Course List:</h2>
		<div class="Course-List-block">
			<ul class="list-group" id="Course List">
			</ul>
		</div>
    </body>
</html>
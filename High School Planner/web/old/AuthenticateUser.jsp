<%-- 
    Document   : home
    Created on : Mar 24, 2015, 12:23:01 AM
    Author     : tomaszGolebiowski
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
		<link href = "css/bootstrap.min.css" rel = "stylesheet">
        <link href = "css/styles.css" rel = "stylesheet">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
		
		<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src = "js/bootstrap.js"></script>
    </head>
    <body>
	
		<div class = "navbar navbar-inverse navbar-static-top">
			<div class = "container">
				<div class = "navbar-header">

                    <a href = "AuthenticateUser" class = "navbar-brand">Welcome to High School Planner</a>
                    <button class = "navbar-toggle" data-toggle = "collapse" data-target = ".navHeaderCollapse">
                        <span class = "icon-bar"></span>
                    </button>
					
                </div>

            </div>
        </div>
	
		<form class="form-horizontal" action="LogInServlet" method="POST">
			<div class="form-group">
				<label class="control-label col-sm-2" for="username">Username:</label>
				<div class="col-sm-10">
					<input type="username" class="form-control" id="username" name="usename" placeholder="Username">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="pwd">Password:</label>
				<div class="col-sm-10">          
					<input type="password" class="form-control" id="pwd" name="password" placeholder="Enter password">
				</div>
			</div>
			<div class="form-group">        
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" id="submitButton" class="btn btn-default">Submit</button>
				</div>
			</div>
			<label class="control-label col-sm-2" for="RequestAccount"><a href="#">Request an Account</a></label>
		</form>
    </body>
</html>
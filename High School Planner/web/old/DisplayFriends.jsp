<%-- 
    Document   : DisplayFriends
    Created on : Mar 24, 2015, 2:02:10 AM
    Author     : richaprajapati
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Display Friends</title>
    </head>
    <body>
	 <div class = "container">
        <div class = "navbar-header">
            <a href = "#" class = "navbar-brand">High School Planner</a>
        </div>
			<div class = "collapse navbar-collapse navHeaderCollapse">
                <ul class = "nav navbar-nav navbar-right">

                    <li><a href = "AssignedSchedule.jsp">Assigned Schedule</a></li>
                    <li><a href = "DesiredSchedule.jsp">DesiredSchedule</a></li>
                </ul>
            </div>
        </div>
	
        <div id="FriendList">
            <a href="">Richa Prajapati</a>
            <a href="">Tomasz G.</a>
            <a href="">Angela M.</a>
            <a href="">Brian B.</a>
        </div>
    </body>
</html>

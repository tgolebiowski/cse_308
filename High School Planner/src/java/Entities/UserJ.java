package Entities;




import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import static javax.persistence.InheritanceType.*;
import javax.persistence.Table;

@Entity
@Table(name = "USERJ")
@Inheritance(strategy=SINGLE_TABLE)

@DiscriminatorColumn(name="DISCRIMINATOR")

public class UserJ implements Serializable {

    @Basic
    private String firstName;
    @Basic
    private String lastName;
   
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int userId;
   
    @Basic
    private String password;

    public UserJ() {

    }
    
    public UserJ(String firstName, String lastName, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
    }
   
    public String getFirstName() {
        return this.firstName;
    }
    
    public String getLastName(){
        return this.lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public void setLastName(String lastName){
        this.lastName = lastName;
    }
   
    public int getUserId() {
        return this.userId;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

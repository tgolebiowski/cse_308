/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.List;
import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.Basic;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author richaprajapati
 */
@Entity
@DiscriminatorValue(value = "AssignedSchedule")
public class AssignedSchedule extends Schedule implements Serializable {

    public AssignedSchedule() {
        this.sections = new ArrayList<Section>();
    }

    @Basic
    private int schoolYear;

    @OneToOne(cascade = ALL)
    private Semester startSemester;

    @OneToOne(cascade = ALL)
    private Semester endSemester;

    ////////TAKEN AS SORT OF SCHEDULE BLOCK ////
    @OneToMany(cascade = ALL)
    private List<Section> sections;

    @Basic
    private String scheduleBlock;

    public int getYear() {
        return this.schoolYear;
    }

    public void setYear(int schoolYear) {
        this.schoolYear = schoolYear;
    }

    public Semester getStartSemester() {
        return this.startSemester;
    }

    public void setStartSemester(Semester startSemester) {
        this.startSemester = startSemester;
    }

    public Semester getEndSemester() {
        return this.endSemester;
    }

    public void setEndSemester(Semester endSemester) {
        this.endSemester = endSemester;
    }

    public List getSections() {
        return this.sections;
    }

    public void setSections(List sections) {
        this.sections = sections;
    }

    public void setInstructor(List sections) {
        this.sections = sections;
    }

    ///////////////CHANGE THIS //////////////
    public String getScheduleBlock() {
        return this.scheduleBlock;
    }

    public void setScheduleBlock(String scheduleBlock) {
        this.scheduleBlock = scheduleBlock;
    }
}

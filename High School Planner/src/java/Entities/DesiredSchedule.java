/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.List;
import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.Basic;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 *
 * @author richaprajapati
 */
@Entity
@DiscriminatorValue(value = "DesiredSchedule")
public class DesiredSchedule extends Schedule implements Serializable{
 
   
    @OneToMany(cascade=ALL)
    private List<Section> courses;
    
    @OneToMany(cascade=ALL)
    private List<Section> excludedSections;
    
    //each index correpsonds to a day
    //i.e. Monday 0, Tuesday 1, Wednesday 2 etc.
    @Basic
    private List<String> lunchPeriods;
    public DesiredSchedule()
    {
        this.excludedSections =  new ArrayList<Section>();
        this.lunchPeriods = new ArrayList();
        this.courses = new ArrayList();
    }
    
    public List getExcludedSections()
    {
        return this.excludedSections;
    }
    
    public void setExcludedSections(List excludedSections)
    {
        this.excludedSections = excludedSections;
    }
    
     public List getLunchPeriods()
    {
        return this.lunchPeriods;
    }
    
    public void setLunchPeriods(List lunchPeriods)
    {
        this.lunchPeriods = lunchPeriods;
    }
   
}

package Entities;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//@Entity
@javax.persistence.Entity(name = "SchoolEntity")
@Table(name = "SchoolTable")
public class School implements Serializable {

	//private ArrayList<Timeslot> timeslotList;
       @OneToMany(cascade=ALL)
	private List<Semester> semesterList = new ArrayList<Semester>();
	
        @Basic
         private String schoolName;
       
       @Basic
       private int semesterCount;
       
        @Basic
	private Semester currentSemester;
        

         //@javax.persistence.Id;
         //@javax.persistence.Access.GeneratedValue(strategy = Generation.IDENTITY)

	   @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

//s@javax.persistence.GeneratedValue(strategy = GenerationType.IDENTITY)
        private long schoolID ;
        
         @Basic
	private int currentYear = Calendar.YEAR;
	
          @Basic
         private int currentSemesterIndex;
          
          @Basic
         private int numOfPeriods;
          
          @Basic
         private int numOfDays;
	
	public School(){
            
        }
        
        public School( String name, int semesterCount, int currentYear, int numOfPeriods, int numOfDays){
            
            this.semesterCount = semesterCount;
            this.currentYear = currentYear;
            this.schoolName = name;
            this.numOfDays = numOfDays;
            this.numOfPeriods = numOfPeriods;
            this.currentSemesterIndex = 1;
        }
/*
	public ArrayList<Timeslot> getTimeslotList() {
		return timeslotList;
	}

	public void setTimeslotList(ArrayList<Timeslot> timeslotList) {
		this.timeslotList = timeslotList;
	}*/

	public int getSemesterCount() {
		return semesterCount;
	}

	public void setSemesterCount(int semesterCount) {
		this.semesterCount = semesterCount;
	}
	
	public void setCurrentSemester(Semester currentSemester) {
		this.currentSemester = currentSemester;
	}

	public List<Semester> getSemesterList() {
		return semesterList;
	}

	public Semester getCurrentSemester() {
		return currentSemester;
	}

	public long getSchoolID() {
		return schoolID;
	}
        
        public String getSchoolName()
        {
            return this.schoolName;
        }
        
        public void setSchoolName(String name)
        {
            this.schoolName = name;
        }
        
         public int getNumOfPeriods()
        {
            return this.numOfPeriods;
        }
        
        public void setNumOfPeriods(int numOfPeriods)
        {
            this.numOfPeriods = numOfPeriods;
        }
	
         public int getNumOfDays()
        {
            return this.numOfDays;
        }
        
        public void setNumOfDays(int numOfDays)
        {
            this.numOfDays = numOfDays;
        }
	
	//creates new semester; can do so with previous semester's 
	//	student, instructor, and course lists, or start from scratch
	/*
        public void createSemester(boolean fromScratch){
		if (semesterList.size()==0){
			semesterList.add(new Semester(currentYear, semesterIndex));
			currentSemester = semesterList.get(0);
		}
		else if(fromScratch)
			semesterList.add(new Semester(currentYear, semesterIndex));
		else{
			Semester lastSemester = semesterList.get(semesterList.size()-1);
			semesterList.add(new Semester(lastSemester.getStudentList(),
					lastSemester.getCourseList(), lastSemester.getInstructorList(),
					currentYear, semesterIndex));
		}
		semesterIndex++;
		if (semesterIndex > semesterCount){
			semesterIndex = 0;
			currentYear++;
		}
	}
	
	public boolean equals(School s){
		return (s.getSchoolID() == schoolID);
	}*/
	
}

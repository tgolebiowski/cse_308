package Entities;




import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue(value = "Admin")
public class Admin extends UserJ implements Serializable {

    public Admin() {

    }
    public Admin(String firstName, String lastName, String password)
    {
        super(firstName, lastName, password);
    }
   
}

package Entities;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Course implements Serializable {
	
    public Course()
        {
        
        }
    
      @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int courseId;
    
    @OneToMany(cascade=ALL,mappedBy="course")
    private List<Section> sections = new ArrayList<Section>();
    
    @Basic
    private String courseName;
    
    @OneToOne
    private School school;
    
    public Course(String courseName,List sections)
    {
        this.courseName = courseName;
        this.sections = sections;
             
    }
    public Course(String courseName)
    {
        this.courseName = courseName;
             
    }
    
    public int getID(){return courseId;}
	public String getCourseName(){return courseName;}
        
        public void setCourseName(String courseName)
        {
            this.courseName = courseName;
        }
        
        public List getSections()
        {
            return this.sections;
        }
        
        public void setSections(List sections)
        {
            this.sections = sections;
        }
        
        public School getSchool()
        {
            return this.school;
        }
        
        public void setSchool(School school)
        {
            this.school = school;
        }
	
    
}

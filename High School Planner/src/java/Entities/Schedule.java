package Entities;




import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import static javax.persistence.FetchType.LAZY;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="SCHEDULE_DISCRIMINATOR")
public class Schedule implements Serializable {

    public Schedule()
    {
        
    }
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long ScheduleId;
    
    @OneToMany(cascade=ALL,  fetch=LAZY)
    private List<Course> courses = new ArrayList<Course>();

   
  
    public Schedule(List courses)
    {
        this.courses = courses;
    }
   
    public Long getScheduleId() {
        return this.ScheduleId;
    }

    public void setCourses(ArrayList courses)
    {
         this.courses = courses;
    }
    
    public List getCourses(ArrayList courses)
    {
        return this.courses;
    }
    
   
}

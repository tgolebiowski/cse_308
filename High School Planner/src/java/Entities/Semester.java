package Entities;




import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Semester implements Serializable {
	
        @OneToMany(cascade=ALL)
        private List<Student> studentList = new ArrayList<Student>();
        
        @OneToMany(cascade=ALL)
	private List<Course> courseList = new ArrayList<Course>();
        
        @OneToMany(cascade=ALL)
	private List<Instructor> instructorList = new ArrayList<Instructor>();
	
        @Basic
        private int semesterYear;
	
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private int semesterID;
        
        @Basic
	private int semesterIndex;
	
        public Semester(){}
        
	public Semester(List<Student> sl, List<Course> cl,
			List<Instructor> il, int y, int si){
		studentList = sl;
		courseList = cl;
		instructorList = il;
		semesterYear = y;
		semesterIndex = si;
		
	}
	
	public Semester(int y, int si){
		semesterYear = y;
		semesterIndex = si;
	}
//getters
	public List<Student> getStudentList() {
		return studentList;
	}

	public List<Course> getCourseList() {
		return courseList;
	}

	public List<Instructor> getInstructorList() {
		return instructorList;
	}

	public int getSemesterID() {
		return semesterID;
	}

	public int getSemesterIndex() {
		return semesterIndex;
	}
        
        public void setStudentList(List studentList) {
		this.studentList =  studentList;
	}

	public void setCourseList(List courseList) {
		this.courseList =  courseList;
	}

	public void setInstructorList(List instructorList) {
		this.instructorList = instructorList;
	}

	
	public void setSemesterIndex(int semesterIndex) {
		this.semesterIndex = semesterIndex;
	}
        
	/*
	//adds student, if not already added
	public boolean addStudent(Student s){
		if (isRegistered(s) == -1){
			studentList.add(s);
			return true; }
		else return false;	
	}
	
	//adds instructor, if not already added
	public boolean addInstructor(Instructor i){
		if (isRegistered(i) == -1){
			instructorList.add(i);
			return true; }
		else return false;	
	}
	
	//adds course, if not already added
	public boolean addCourse(Course c){
		if (isRegistered(c) == -1){
			courseList.add(c);
			return true; }
		else return false;	
	}
	
	//removes student, if found
	public boolean removeStudent(Student s){
		int index = isRegistered(s);
		if (index == -1){
			return false; }
		studentList.remove(index);
		return true;
	}
	
	//removes instructor, if found
	public boolean removeInstructor(Instructor i){
		int index = isRegistered(i);
		if (index == -1){
			return false; }
		instructorList.remove(index);
		return true;
	}
	
	//removes course, if found
	public boolean removeCourse(Course c){
		int index = isRegistered(c);
		if (index == -1){
			return false; }
		courseList.remove(index);
		return true;
	}
	
	//checks if student has been added
	public int isRegistered(Student s){
		for (int i = 0; i < studentList.size(); i++)
			if (studentList.get(i).equals(s)) return i;
		return -1;
	}
	
	//checks if course has been added
	public int isRegistered(Course c){
		for (int i = 0; i < courseList.size(); i++)
			if (courseList.get(i).equals(c)) return i;
		return -1;
	}
	
	//checks if instructor has been added
	public int isRegistered(Instructor i){
		for (int j = 0; j < instructorList.size(); j++)
			if (instructorList.get(j).equals(i)) return j;
		return -1;
	}
	
	public boolean equals(Semester s){
		return (s.getSemesterID() == semesterID);
	}*/
}

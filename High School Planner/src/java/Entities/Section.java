package Entities;




import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Section implements Serializable {
	
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private int sectionId;
        
        @ManyToOne(cascade=ALL)
	private Instructor instructor;
        
        @OneToOne(cascade=ALL)
        private Course course;
        
        @OneToMany(cascade=ALL)
	private List<Student> students = new ArrayList<Student>();
	//private Timeslot time;
	@Basic
        private int capacity;
	
       
        
	
        public Section(){}
        
	public Section(Instructor instructor, Course course, List students)
        {
            this.instructor = instructor;
            this.course = course;
            this.students = students;
        }
        
	//getters
	public Instructor getInstructor(){return instructor;}
	public List<Student> getStudents(){return students;}
	//public Timeslot getTimeslot(){return time;}
	public int getCapacity(){return capacity;}
	public int getID(){return sectionId;}
	public Course getCourse(){return course;}
        
        public void setCourse(Course course)
        {
            this.course = course;
        }
	//setters
	public void setInstructor(Instructor i){instructor = i;}
	//public void setTimeslot(Timeslot t){time = t;}
	public void setCapacity(int c){capacity = c;}
	
        /*
	//Adds student to course if course is not full and student is not already registered
	public boolean addStudent(Student s){
		if (!isFull() && isRegistered(s) == -1){
			students.add(s);
			return true; }
		else return false;	
	}
	
	//Removes the student if the student is registered
	public boolean removeStudent(Student s){
		int index = isRegistered(s);
		if (index == -1){
			return false; }
		students.remove(index);
		return true;
	}
	
	//public boolean isAtTime(Timeslot t){return time.equals(t);}
	
	//Checks if course is full
	public boolean isFull(){return !(students.size() == capacity);}
	
	//Checks if student is registered and returns its index in the list
	public int isRegistered(Student s){
		for (int i = 0; i < students.size(); i++)
			if (students.get(i).equals(s)) return i;
		return -1;
	}
	
	public boolean equals(Course c){return sectionId == c.getID();} */
	
	
}

package Entities;




import javax.persistence.Basic;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author mo
 */
public class Timeslot {
    
    @Basic
    private double startTime;
   
    @Basic
    private double endTime;
    
    @Basic
    private int dayPattern;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int timeslotID;
    
    public Timeslot(double startTime, double endTime, int dayPattern){
        this.startTime = startTime;
        this.endTime = endTime;
        this.dayPattern = dayPattern;
    }
    
    /*Getter Setter methods*/
    public double getStartTime() {
        return startTime;
    }

    public void setStartTime(double startTime) {
        this.startTime = startTime;
    }

    public double getEndTime() {
        return endTime;
    }

    public void setEndTime(double endTime) {
        this.endTime = endTime;
    }

    public int getDayPattern() {
        return dayPattern;
    }

    public void setDayPattern(int dayPattern) {
        this.dayPattern = dayPattern;
    }

    public int getTimeslotID() {
        return timeslotID;
    }

    public void setTimeslotID(int timeslotID) {
        this.timeslotID = timeslotID;
    }
    
    
    
}

package Entities;




import java.io.Serializable;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class FriendRequest implements Serializable {

   
      @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long friendRequestId;
    
      
    public enum RequestStatus { New, Accepted, Denied }
    
    @OneToOne(cascade=ALL)
    private Student studentSent;
    
    @OneToOne(cascade=ALL)
    private Student studentReceived;
    
    @Enumerated(EnumType.STRING)
    public RequestStatus status;

    public FriendRequest() {

    }
   
    public FriendRequest(Student sent, Student received) {
		
		studentSent = sent;
		studentReceived = received;
		//status = RequestStatus.New;
	}
	/*
    public void accept() {
		status = RequestStatus.Accepted;
	}
	
    public void deny() {
		status = RequestStatus.Denied;
	}*/
    
    public Student getStudentSent() {
        return this.studentSent;
    }

    public void setStudentSent(Student studentSent) {
        this.studentSent = studentSent;
    }
    
    public Student getStudentReceived() {
        return this.studentSent;
    }

    public void setStudentReceived(Student studentReceived) {
        this.studentSent = studentReceived;
    }
   
     
    public Long getFriendRequestId() {
        return this.friendRequestId;
    }

    public void setRequestStatus(RequestStatus status) {
        this.studentSent = studentReceived;
    }
   
     
    public RequestStatus getRequestStatus() {
        return this.status;
    }
   
    
}

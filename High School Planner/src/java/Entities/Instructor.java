package Entities;




import Entities.Section;
import java.io.Serializable;
import javax.persistence.*;
import static javax.persistence.CascadeType.*;
import static javax.persistence.FetchType.*;
import java.util.*;
import static javax.persistence.InheritanceType.SINGLE_TABLE;

@Entity

public class Instructor implements Serializable {
  @Id
 @GeneratedValue(strategy = GenerationType.AUTO)
private int id;

 @Basic
 private String name;
 public Instructor() {
     

 }
 public Instructor(String name) {

 this.name = name;
 }
 public Instructor(String name,List sections) {
 this.sections = sections;
 this.name = name;
 }

@OneToMany(cascade=ALL,mappedBy="instructor")
private List<Section> sections  = new ArrayList<Section>();

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the Courses
     */
    public List<Section> getCourses() {
        return sections;
    }

    /**
     * @param Courses the Courses to set
     */
    public void setCourses(List<Section> sections) {
        this.sections = sections;
    }
 



 }
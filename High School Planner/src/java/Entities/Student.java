package Entities;




import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue(value = "Student")
public class Student extends UserJ implements Serializable {

    @OneToMany
    private List<FriendRequest> friendRequests = new ArrayList<FriendRequest>();
    
    @OneToOne
    private AssignedSchedule assignedSchedule;
    
     @OneToOne
    private DesiredSchedule desiredSchedule;
    
     @Basic
     private boolean accountApproved;
     
    @ManyToMany
    private List<School> school =new ArrayList<School>();
   
    @OneToMany
    private List<Instructor> preferredInstructors = new ArrayList<Instructor>();
    
    @Basic
    private String academicLevel;

    public Student() {

    }
    
    public Student(List schools, List instructors, List friendRequests)
    {
        this.school = schools;
        this.preferredInstructors = instructors;
        this.friendRequests = friendRequests;
    }
   
    public Student(String firstName, String lastName, String password)
    {
        super(firstName, lastName ,password);
    }
    
    public List<FriendRequest> getFriendRequests() {
        return this.friendRequests;
    }

    public void setFriendRequests(List<FriendRequest> friendRequests) {
        this.friendRequests = friendRequests;
    }
   
    public List<School> getSchool() {
        return this.school;
    }

    public void setSchool(List<School> school) {
        this.school = school;
    }
   
    public Collection<Instructor> getPreferredInstructors() {
        return this.preferredInstructors;
    }

    public void setPreferredInstructors(List<Instructor> preferredInstructors) {
        this.preferredInstructors = preferredInstructors;
    }
   
    public AssignedSchedule getAssignedSchedule() {
        return this.assignedSchedule;
    }

    public void setAssignedSchedule(AssignedSchedule assignedSchedule) {
        this.assignedSchedule = assignedSchedule;
    }
   
    public DesiredSchedule getLastGeneratedSchedule() {
        return this.desiredSchedule;
    }

    public void setLastGeneratedSchedule(DesiredSchedule lastGeneratedSchedule) {
        this.desiredSchedule = lastGeneratedSchedule;
    }
   
    public String getAcademicLevel() {
        return this.academicLevel;
    }

    public void setAcademicLevel(String academicLevel) {
        this.academicLevel = academicLevel;
    }
    
    public boolean isApproved() {
        return this.accountApproved;
    }

    public void setApprovalStatus(boolean approve) {
        this.accountApproved = approve;
    }
}

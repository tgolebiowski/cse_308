/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Entities.Admin;
import Entities.Student;
import JavaBeans.HighSchoolBean;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author richaprajapati
 */
public class LogInServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            //Get username and password
            String username =request.getParameter("usename");
            String password = request.getParameter("password");
            
            //Call bean to create entity manager
            HighSchoolBean bean = new HighSchoolBean();
            
            //Get the First name and Last name from the username and using that get the user
            Object user = bean.getUser(username.split(" ")[0] , username.split(" ")[1]);
            
            //Check if the user is Student or Admin
            if(user instanceof Student || 
                    user.getClass().toString().equalsIgnoreCase("class entity.Student"))
            {
                //Save the username for the session
                request.getSession().setAttribute("username",username);
                
                //Render the student home page
                response.sendRedirect("Student_Home.jsp");
            }
            else if(user instanceof Admin || 
                    user.getClass().toString().equalsIgnoreCase("class entity.Admin"))
            {
                //Save the username for the session
                request.getSession().setAttribute("username",username);
                
                //Render the Admin home page
                response.sendRedirect("Admin_Home.jsp");
            }
            else
            {
                //If the user was not found, show the same page with a 
                //message saying user not found
                response.sendRedirect("AuthenticateUser.jsp");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Entities.AssignedSchedule;
import Entities.Course;
import Entities.Instructor;
import Entities.Section;
import Entities.Semester;
import Entities.Student;
import JavaBeans.FriendsBean;
import JavaBeans.HighSchoolBean;
import java.util.List;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author richaprajapati
 */
public class EnterAssignedScheduleServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    PrintWriter out = response.getWriter();
          //request.getSession().invalidate();
    
     
            HighSchoolBean bean = new HighSchoolBean();
            //EntityManager em = bean.createTransactionalEntityManager();
            
            AssignedSchedule schedule = new AssignedSchedule();
            bean.addEntity(schedule);
           
            Student student = (Student)bean.getEntity("Student","John");
            //Put this in session
            
            String[] courseNames = request.getParameterValues("courseName");
            String endSemester = request.getParameter("endSemester");
            String startSemester = request.getParameter("endSemester");
            String[] instructors = request.getParameterValues("instructor");
            //   String[] scheduleBlocks = request.getParameterValues("endSemester");
            String schoolYear = request.getParameter("schoolYear");
           
            List<Section> sections = new ArrayList<Section>();
            
            ///////////// CHECK FOR CONSTRAINTS ///////////////
            for(int i=0; i<courseNames.length; i++)
            {
                Course course;
                Section section = new Section();
                bean.addEntity(section);
                //If the course already exists in the database, simply assign it to the section
                if((course = (Course)bean.getCourse("Course", courseNames[i])) != null && courseNames[i] != null)
                {
                    section.setCourse(course);
                }
                else
                {
                    course = new Course();
                    bean.addEntity(course);
                    course.setCourseName(courseNames[i]);
                    section.setCourse(course);
                }
                
                Instructor instructor;
                if((instructor = (Instructor)bean.getInstructor("Instructor", instructors[i])) != null && instructors[i] != null)
                {
                    section.setInstructor(instructor);
                }
                else
                {
                    instructor = new Instructor();
                    bean.addEntity(instructor);
                    instructor.setName(instructors[i]);
                    section.setInstructor(instructor);
                }
                
                sections.add(section);
            }
            
           // schedule.setEndSemester((Semester)bean.getSemester("Semester", endSemester));
            //schedule.setStartSemester((Semester)bean.getSemester("Semester", startSemester));
            schedule.setScheduleBlock(null);
            schedule.setYear(Integer.parseInt(schoolYear));
            schedule.setSections(sections);
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("</html>");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

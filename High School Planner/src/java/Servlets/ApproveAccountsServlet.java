/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Entities.Student;
import JavaBeans.HighSchoolBean;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author richaprajapati
 */
public class ApproveAccountsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
           
           
            String [] approveAccounts = 
                    request.getParameterValues("approveAccountList[]");
            String [] deleteAccounts = 
                    request.getParameterValues("deleteAccountList[]");
            
            HighSchoolBean bean = new HighSchoolBean();
            
            //Add each element in the list of approveAccounts into the database
            for(int i=0; i< approveAccounts.length; i++)
            {
               Student s = (Student)bean.getStudent( 
                       Integer.parseInt(approveAccounts[i]));
               s.setApprovalStatus(true);
               bean.merge(s);
            }
            
            //Delete each account correponding to the accounts in deleteAccounts 
            //from the database
            for(int i=0; i< approveAccounts.length; i++)
            {
               Student s = (Student)bean.getStudent(
                       Integer.parseInt(approveAccounts[i]));
               bean.removeStudent(s);
            }
            response.setStatus(200);
           

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Entities.Student;
import JavaBeans.AccountBean;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mo
 */
public class RequestAccountServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        
        try (PrintWriter out = response.getWriter()) {
        String firstName = request.getParameter("FirstName");
        String lastName = request.getParameter("LastName");
        String school = request.getParameter("SchoolName");
        String email = request.getParameter("Email");
        String password = request.getParameter("Password");
        String confirmPass = request.getParameter("ConfirmedPassword");
        
        AccountBean requestNew = new AccountBean();
        boolean checkAccount = requestNew.checkPassCorrect(
                password, confirmPass);
        if(checkAccount == true){
            Student new1 = requestNew.requestAccount(
                    firstName, lastName, school, password, confirmPass);
            
            //An instance of student is created.
            //If later, this account request was declined by the admin, 
            //the object will  be deleted
            requestNew.addEntity(new1);
            
            //Reloads the request account page with a message 
            //indicating the successful request of an account 
            request.setAttribute("AccountRequested", "success");
            response.sendRedirect("RequestAccount.jsp");
        }
        else{
            //Reload the page showing a message of incorrect id 
            //or password
            request.setAttribute("AccountReuested", "error");
            response.sendRedirect("RequestAccount.jsp");
        }

       }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

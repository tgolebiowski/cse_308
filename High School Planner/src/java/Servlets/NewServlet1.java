/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Entities.Course;
import Entities.Instructor;
import Entities.School;
import Entities.Section;
import Entities.Semester;
import Entities.Student;
import JavaBeans.HighSchoolBean;
import JavaBeans.NewClass;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static sun.net.www.http.HttpClient.New;

/**
 *
 * @author richaprajapati
 */
public class NewServlet1 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
  //  NewClass nc = new NewClass();
  //   private static final String PERSISTENCE_UNIT_NAME = "High_School_PlannerPU";
   // private static final EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
    //private static  EntityManager em ;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        // em = factory.createEntityManager();
           
             HighSchoolBean bean = new HighSchoolBean();
             EntityManager em = bean.createTransactionalEntityManager();
             Student student = new Student();
             student.setFirstName("John");
             student.setLastName("Smith");
             student.setPassword("badPassword");
             em.persist(student);
              Student student1 = new Student();
             student1.setFirstName("Richa");
             student1.setLastName("P");
             student1.setPassword("badPassword");
             em.persist(student1);
              Student student2 = new Student();
             student2.setFirstName("Tom");
             student2.setLastName("G");
             student2.setPassword("badPassword");
             em.persist(student2);
              Student student3 = new Student();
             student3.setFirstName("Angela");
             student3.setLastName("M");
             student3.setPassword("badPassword");
             em.persist(student3);
            School school = new School();
            em.persist(school);
            school.setSchoolName("Stony Brook");
            Instructor instructor = new Instructor();
             em.persist(instructor);
             instructor.setName("Scott Stoller");
            Course course = new Course();
             em.persist(course);
            Section section = new Section();
             em.persist(section);
            Semester semester = new Semester();
             em.persist(semester);
        //school.setId(2);
        school.setSemesterCount(4);
        
        
        course.setCourseName("CSE308");
        course.setSchool(school);
        
       
        section.setCapacity(20);
        section.setCourse(course);
        section.setInstructor(instructor);
        List<Section> sections = new ArrayList<Section>();
        sections.add(section);
        course.setSections(sections);
        
        List<Course> courses = new ArrayList<Course>();
        courses.add(course);
        
         List<Instructor> instructors = new ArrayList<Instructor>();
        instructors.add(instructor);
        
        
       
        semester.setCourseList(courses);
        semester.setInstructorList(instructors);
        semester.setSemesterIndex(3);
        school.setCurrentSemester(semester);
        
        bean.closeTransactionalEntityManager();
        //bean.addEntity(school);
        
       /* Student student = new Student();
        student.setFirstName("John");
        student.setLastName("Smith");
        student.setPassword("badPassword");
       */
        //This will cretae the entitymanager, persist the entity, commit the transaction and close the entitymanager
       // bean.addEntity(student);
           
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet NewServlet1</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet "+school.getSchoolID()+"NewServlet1 at " + request.getParameter("numOfSemesters")+ "</h1>");
            out.println("</body>");
            out.println("</html>");
           
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

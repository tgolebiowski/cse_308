/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Entities.AssignedSchedule;
import Entities.DesiredSchedule;
import Entities.Student;
import JavaBeans.HighSchoolBean;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author richaprajapati
 */
public class ViewScheduleServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
           String scheduleType = request.getParameter("shceduleType");
           HighSchoolBean bean = new HighSchoolBean();
          out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CreateFriendServlet</title>");            
            out.println("</head>");
            out.println("<body>");
           
           if(scheduleType.equals("desiredSchedule"))
           {
               DesiredSchedule schedule = ((Student)bean.getStudent(1)).getLastGeneratedSchedule();
                out.println("<h1>Servlet CreateFriendServlet at " + schedule.getScheduleId() + "</h1>");
           
           }
           else if(scheduleType.equals("assignedSchedule"))
           {
               AssignedSchedule schedule = ((Student)bean.getStudent(1)).getAssignedSchedule();
                out.println("<h1>Servlet CreateFriendServlet at " + schedule.getScheduleId() + "</h1>");
           
           }
            out.println("</body>");
            out.println("</html>");
           
        }  
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

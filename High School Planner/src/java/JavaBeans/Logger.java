package JavaBeans;



import java.io.IOException;
import java.io.*;
/*
 * The logging class!
 * 
 * To use: simply create a Logger object before you begin tests
 * then, as needed, call Logger.logString with the desired string to be logged
 * once you have reached the end of your tests, be sure to call Logger.endLogging
 * to close the logging file 
 * (end step not terribly important if the testing app exits soon after, but good practice)
 * 
 * Note on output file:
 * Logging is saved to a file called "LastRunLog.txt", 
 * if the file does not exist in the working directory, it will create it.
 * If the file does exist, Logger WILL OVERWRITE ANYTHING INSIDE, 
 * so be sure to save any logs of previous runs.
 */
public class Logger {

	private FileWriter log;

	public Logger() {
		try {
                    log = new FileWriter("LastRunLog.txt", false);
                }
                catch(IOException e) {
                    System.out.println("Error creating logger.");
                }
	}

	public void logString(String string) {
		try {
			log.write(string);
			log.write("\r\n");
		}
		catch(IOException e) {
			System.out.println("IOException in logger (was logging already ended?) ");
                        
                        try {
                            log.close();
                        }
                        catch(IOException ioe) {
                            //squash exceptions
                        }
		}
	}

	public void endLogging() {
		try {
                    log.close();
                }
                catch(IOException ioe) {
                    //squash exceptions
                }
	}

}
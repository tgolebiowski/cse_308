/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaBeans;

import Entities.UserJ;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author richaprajapati
 */
public class NewClass {
    private static final String PERSISTENCE_UNIT_NAME = "High_School_PlannerPU";
    private static final EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
    private static  EntityManager em ;
    
    public NewClass()
    {
           em = factory.createEntityManager();
           
    }
    
    public void add(Object o){
    //Log this
         
        em.getTransaction().begin();
        em.persist(o);
        System.out.println("Entity added");
        em.getTransaction().commit();
        em.close();
          
    }
            
    
    public EntityManager getEm()
    {
        return em;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaBeans;


import Entities.Student;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author mo
 */
@Stateless
public class AccountBean {

    private static final String PERSISTENCE_UNIT_NAME = "High_School_PlannerPU";
    //private static final EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
    private EntityManagerFactory factory;
    private EntityManager em;
    
    private static Logger logger = new Logger();

    public AccountBean() {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);

    }
    
    //*Note student class constructor doesn't do anything with e-mail
    public Student requestAccount(String firstName, String lastName, String school, String password, String confirmPass){
        //create new Student object with all these inputs
        logger.logString("Making a new Student account up for request");
        Student student = new Student(firstName, lastName, password);
        student.setSchool(null); //set to school from List of school in database

        return student;
        
    }
    
    public boolean checkPassCorrect(String password, String confirmPass){
        logger.logString("check if the password and confirm password are the same");
        if(password.equals(confirmPass)){
            return true;
        }
        else{
            return false;
        }
    }
    

    public void addEntity(Object entity) {
        logger.logString("Entity added");
        em = factory.createEntityManager();

        if (em.getTransaction() != null) {
            em.getTransaction().begin();
            em.persist(entity);
            //em.flush();
            em.getTransaction().commit();
            em.close();
            em = null;

        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaBeans;

import Entities.Student;
import static com.sun.xml.ws.security.addressing.impl.policy.Constants.logger;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author richaprajapati
 */
@Stateless
public class HighSchoolBean {

    private static final String PERSISTENCE_UNIT_NAME = 
                                "High_School_PlannerPU";
    private EntityManagerFactory factory;
    private EntityManager em;
<<<<<<< HEAD
    private static Entities.Logger logger = new Entities.Logger();
=======
    
    private static Logger logger = new Logger();
>>>>>>> dbdea31e3ce5588e68cc8f59cb1e8a720b9f6087

    public HighSchoolBean() {
        
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
    }

    public EntityManager getEntityManager() {
        logger.logString("get entity");
        //logger.logString("Making a new Student account up for request");
        return em;
    }

    public void close() {
        logger.logString("close");
        em.close();
        em = null;

    }

    public Object merge(Object entity) {
<<<<<<< HEAD
        logger.logString("Merging entities.");
=======
        logger.logString("Merge");
>>>>>>> dbdea31e3ce5588e68cc8f59cb1e8a720b9f6087
        em = factory.createEntityManager();
        em.getTransaction().begin();
        em.merge(entity);
        em.getTransaction().commit();
        em.close();
        em = null;
        return entity;
    }

    public void addEntity(Object entity) {
<<<<<<< HEAD
        //Log the adding functionality
        logger.logString("adding entity.");

        //Create entity manager
=======
        logger.logString("add entityt");
>>>>>>> dbdea31e3ce5588e68cc8f59cb1e8a720b9f6087
        em = factory.createEntityManager();

        if (em.getTransaction() != null) {
            //beging the transaction
            em.getTransaction().begin();

            //Persist the entity
            em.persist(entity);

            //Comit the transaction
            em.getTransaction().commit();

            //Close entityManager
            em.close();
            em = null;

            //Log the insertion of entity
            logger.logString("Entity added.");

        }
    }

<<<<<<< HEAD
    public boolean removeStudent(Student entity) {
        logger.logString("removing entity.");
        em = factory.createEntityManager();

        //Get refrence of the entity to be removed 
        Student sReference = em.getReference(Student.class, entity.getUserId());

        if (sReference != null) {
=======
     public boolean removeStudent(Student entity) {
         logger.logString("remove student");
       em = factory.createEntityManager();
       
        Student s = em.getReference(Student.class, entity.getUserId());
>>>>>>> dbdea31e3ce5588e68cc8f59cb1e8a720b9f6087
            em.getTransaction().begin();

            //Remove the entity referenced by sReference
            em.remove(sReference);

            //Comit the transaction and close the entityManagaer
            em.getTransaction().commit();
            em.close();
            em = null;

            logger.logString("Entity was successfully removed .");
            return true;
        } else {
            logger.logString("Entity not removed.");
            return false;
        }

    }

    //////////////Need to change this according to the entity
    public Object getEntity(String tableName, String firstName) {
<<<<<<< HEAD
        logger.logString("geting entity.");
=======
        logger.logString("get enttity");
>>>>>>> dbdea31e3ce5588e68cc8f59cb1e8a720b9f6087
        em = factory.createEntityManager();
        em.getTransaction().begin();
        Query q = em.createQuery("select entity from " + tableName + " entity where entity.firstName = :firstName");
        q.setParameter("firstName", firstName);
        Object entity = null;

        if (q.getResultList().size() > 0) {
            entity = q.getResultList().get(0);
          //  em.getTransaction().commit();
            // em.close();
            em.getTransaction().commit();
            em.close();
            em = null;
            logger.logString("Inserting entities.");
            return entity;

        } else {
            logger.logString("Inserting entities.");
            return null;
        }

    }

    public Object getUser(String firstName, String lastName) {
        logger.logString("get user");
        em = factory.createEntityManager();
        em.getTransaction().begin();
        Query q = em.createQuery("select entity from UserJ entity where entity.firstName = :firstName AND entity.lastName = :lastName");
        q.setParameter("firstName", firstName);
        q.setParameter("lastName", lastName);
        Object entity = null;

        if (q.getResultList().size() > 0) {
            entity = q.getResultList().get(0);
          //  em.getTransaction().commit();
            // em.close();
            em.getTransaction().commit();
            em.close();
            em = null;
            logger.logString("got user.");
            return entity;
        } else {
            return null;
        }

    }
<<<<<<< HEAD

    public Object getUserById(String entityClass, int id) {
=======
    
    
     public Object getEntityById(String tableName, int id) {
         logger.logString("get by id entity");
>>>>>>> dbdea31e3ce5588e68cc8f59cb1e8a720b9f6087
        em = factory.createEntityManager();
        em.getTransaction().begin();
        
        //Cretae query
        Query q = em.createQuery("select entity from "+entityClass+" entity where "
                + "entity.userId = :id");
        q.setParameter("id", id);
        Object entity = null;

        if (q.getResultList().size() > 0) {
            entity = q.getResultList().get(0);
            em.getTransaction().commit();
            em.close();
            em = null;
            return entity;
        } else {
            return null;
        }

    }

    public Object getCourse(String tableName, String courseName) {
<<<<<<< HEAD
        em = factory.createEntityManager();
        em.getTransaction().begin();
=======
        logger.logString("get course");
em = factory.createEntityManager();
em.getTransaction().begin();
>>>>>>> dbdea31e3ce5588e68cc8f59cb1e8a720b9f6087
        Query q = em.createQuery("select entity from " + tableName + " entity where entity.courseName = :courseName");
        q.setParameter("courseName", courseName);
        Object entity = null;

        if (q.getResultList().size() > 0) {
            entity = q.getResultList().get(0);
            em.getTransaction().commit();
            em.close();
            em = null;
            logger.logString("got course");
            return entity;
        } else {
            logger.logString("failed in getting course");
            return null;
        }

    }
<<<<<<< HEAD

    public Object getInstructor(String tableName, String instructor) {
        em = factory.createEntityManager();
        em.getTransaction().begin();
=======
     
      public Object getInstructor(String tableName, String instructor) {
          logger.logString("get Instructor");
em = factory.createEntityManager();
em.getTransaction().begin();
>>>>>>> dbdea31e3ce5588e68cc8f59cb1e8a720b9f6087
        Query q = em.createQuery("select entity from " + tableName + " entity where entity.name = :instructor");
        q.setParameter("instructor", instructor);
        Object entity = null;

        if (q.getResultList().size() > 0) {
            entity = q.getResultList().get(0);
            em.getTransaction().commit();
            em.close();
            em = null;
            logger.logString("got instructor.");
            return entity;
        } else {
            return null;
        }

    }
<<<<<<< HEAD

    public Object getSemester(String tableName, String semesterID) {
=======
public Object getSemester(String tableName, String semesterID) {
    logger.logString("get semester");
>>>>>>> dbdea31e3ce5588e68cc8f59cb1e8a720b9f6087
//em.getTransaction().begin();
        Query q = em.createQuery("select entity from " + tableName + " entity where entity.semesterID = :semesterID");
        q.setParameter("semesterID", semesterID);
        Object entity = null;

        if (q.getResultList().size() > 0) {
            entity = q.getResultList().get(0);
          //  em.getTransaction().commit();
            // em.close();
            logger.logString("got semester");
            return entity;
        } else {
            return null;
        }

    }

    public List getEntities(String tableName) {
        logger.logString("get entities");
        if ((em.getTransaction() != null) && em.getTransaction().isActive() == true) {
            em.getTransaction().begin();
        }
        Query q = null;
        List entities = null;
        q = em.createQuery("select entity from " + tableName + " entity"); //c where c.nameID = :name");

        em.getTransaction().commit();

        if (q.getResultList().size() > 0) {
            entities = (List) q.getResultList();
        }

        em.getTransaction().commit();
        logger.logString("got entities.");
        return entities;
    }
<<<<<<< HEAD

    public List getCourses(String schoolName, int year) {

        em = factory.createEntityManager();
        if (em.getTransaction() != null) {
=======
    
     public List getCourses(String schoolName, int year) {
         logger.logString("get courses");
       
         em = factory.createEntityManager();
         if (em.getTransaction() != null) {
>>>>>>> dbdea31e3ce5588e68cc8f59cb1e8a720b9f6087
            em.getTransaction().begin();
        }
        Query q = null;
        List entities = null;
        q = em.createQuery("select entity from Course entity where entity.school.schoolName = :schoolName and entity.school.currentYear = :year"); //c where c.nameID = :name");
        q.setParameter("schoolName", schoolName);
        q.setParameter("year", year);

        if (q.getResultList().size() > 0) {
            entities = (List) q.getResultList();
        }

        em.getTransaction().commit();
        em.close();
        em = null;
        logger.logString("got courses.");
        return entities;
    }

    public void createEntityManager() {
        logger.logString("create enetity manager");
        // Create a new EntityManager
        logger.logString("creating entityManager.");
        em = factory.createEntityManager();
    }

    public void closeEntityManager() {
        logger.logString("close entitty manager");
        // Close this EntityManager
        logger.logString("closing entitymanager.");
        em.close();
    }

    public EntityManager createTransactionalEntityManager() {
<<<<<<< HEAD
        logger.logString("creating entity manager and beginning the transaction.");
=======
        logger.logString("createTransaction em");

>>>>>>> dbdea31e3ce5588e68cc8f59cb1e8a720b9f6087
        // Create a new EntityManager
        em = factory.createEntityManager();
        // Begin transaction
        em.getTransaction().begin();
        return em;
    }

    public void closeTransactionalEntityManager() {
        logger.logString("close trans em");
        // Commit the transaction
        logger.logString("committing the transaction and closing the entity manager.");
        em.getTransaction().commit();
        // Close this EntityManager
        em.close();
    }
<<<<<<< HEAD

    public Object getStudent(int userId) {
        logger.logString("getting student.");
=======
    
    public Object getStudent( int userId)
    {
        logger.logString("get student");
>>>>>>> dbdea31e3ce5588e68cc8f59cb1e8a720b9f6087
        em = factory.createEntityManager();
        em.getTransaction().begin();
        Query q = null;
        Object entity = null;
        q = em.createQuery("select student from Student student where student.userId = :userId "); //c where c.nameID = :name");
        // q.setParameter("scheduleType", scheduleType);
        q.setParameter("userId", userId);

        if (q.getResultList().size() > 0) {
            entity = q.getResultList().get(0);
        }

        em.getTransaction().commit();
        em.close();
        em = null;
        return entity;
    }
<<<<<<< HEAD

    public void closeLogger() {
        logger.endLogging();
    }
=======
    
>>>>>>> dbdea31e3ce5588e68cc8f59cb1e8a720b9f6087
}

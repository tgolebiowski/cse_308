import java.util.ArrayList;


public class Course {
	
	private Instructor instructor;
	private ArrayList<Student> students = new ArrayList<Student>();
	private Timeslot time;
	private int capacity;
	private int courseID;
	
	public Course(Instructor i, Timeslot t, int cap, int id){
		instructor = i;
		time = t;
		capacity = cap;
		courseID = id;
		//TODO Determine if ID is generated randomly, where, and how many digits.
	}
	
	//getters
	public Instructor getInstructor(){return instructor;}
	public ArrayList<Student> getStudents(){return students;}
	public Timeslot getTimeslot(){return time;}
	public int getCapacity(){return capacity;}
	public int getID(){return courseID;}
	
	//setters
	public void setInstructor(Instructor i){instructor = i;}
	public void setTimeslot(Timeslot t){time = t;}
	public void setCapacity(int c){capacity = c;}
	
	//Adds student to course if course is not full
	public boolean addStudent(Student s){
		if (!isFull()){
			students.add(s);
			return true; }
		else return false;	
	}
	
	//Removes the student if the student is registered
	public boolean removeStudent(Student s){
		int index = isRegistered(s);
		if (index == -1){
			return false; }
		students.remove(index);
	}
	
	public boolean isAtTime(Timeslot t){return time.equals(t);}
	
	//Checks if course is full
	public boolean isFull(){return !(students.size() == capacity);}
	
	//Checks if student is registered and returns its index in the list
	public int isRegistered(Student s){
		for (int i = 0; i < students.size(); i++)
			if (students.get(i) == s) return i;
		return -1;
	}
	
	public boolean equals(Course c){return courseID == c.getID();}
	
	
}

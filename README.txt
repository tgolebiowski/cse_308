To get set up: 
git clone https://tgolebiowski@bitbucket.org/tgolebiowski/cse_308.git
then run git pull

That should set you up, to push changes:

(if it's been a while, run git pull again before this)
then git add -A
git commit -m "your message"
git push -u origin master